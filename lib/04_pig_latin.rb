def translate(str)
  output = []
  vowels = ["a", "e", "i", "o", "u"]

  str.split.each do |word|
    if vowels.include?(word[0])
      output << word + "ay"
    elsif word.include?("qu")
      split = word.split("qu")
      if split.first.empty?
        output << split.last + "quay"
      else
        output << split.last + split.first + "quay"
      end
    else
      output << pig_latinize(word)
    end
  end

  output.join(" ")
end

def pig_latinize(str)
  letters = str.chars
  vowels = ["a", "e", "i", "o", "u"]
  consonants, remainder = "", ""

  letters.each_with_index do |ch, idx|
    consonants << ch

    if vowels.include?(letters[idx + 1])
      remainder = str.split(consonants).last
      break
    end
  end

  remainder + consonants + "ay"
end
