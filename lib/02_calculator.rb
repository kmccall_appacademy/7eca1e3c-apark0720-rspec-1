def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(array)
  return 0 if array.empty?
  array.reduce(:+)
end

def multiply(*nums)
  nums.reduce(:*)
end

def power(num1, num2)
  num1**num2
end

def factorial(num)
  int = num
  multiple = num - 1

  while multiple > 0
    int *= multiple
    multiple -= 1
  end

  int
end
