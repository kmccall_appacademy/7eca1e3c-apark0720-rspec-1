def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, times = 2)
  ([string] * times).join(" ")
end

def start_of_word(string, num)
  string.chars.take(num).join
end

def first_word(string)
  string.split.first
end

def titleize(string)
  output = []
  little_words = ["a", "and", "the", "to", "for",
  "by", "in", "out", "up", "down", "at", "with",
  "past", "over", "nor", "but", "or", "yet", "so"]

  string.split.each_with_index do |word, idx|
    output << word.capitalize if idx == 0
    next if idx == 0
    if little_words.include?(word)
      output << word
    else
      output << word.capitalize
    end
  end

  output.join(" ")
end
